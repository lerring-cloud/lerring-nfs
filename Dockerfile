FROM lerring/lerring-base:latest

MAINTAINER Lerring

RUN apt-get install nfs-kernel-server nfs-common rpcbind -y
RUN apt-get install inotify-tools -y
RUN chmod 775 /mnt 
ADD ./etc/ /etc
ADD ./run.sh /run.sh
RUN mkdir /run/sendsigs.omit.d
RUN mkdir /run/sendsigs.omit.d/rpcbind 

RUN systemctl enable rpcbind
RUN rpcbind
#CMD service rpcbind start
#RUN sudo mount -v  -t nfsd nfsd /proc/fs/nfsd
#RUN service  rpc.nfsd restart

#CMD service nfs-kernel-server start
CMD /run.sh

EXPOSE  2049:2050/tcp
EXPOSE  111:112/udp
